package mx.edu.itm.link.t_login.BaseDeDatos

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface UsuarioDao {
    @Query("SELECT * FROM usuarios")
    fun getAll(): LiveData<List<Usuario>>

    @Query("SELECT * FROM usuarios WHERE id_alumno = :id")
    fun get(id: Int): LiveData<Usuario>

    @Insert
    fun insertAll(vararg productos: Usuario)

    @Update
    fun update(producto: Usuario)

    @Delete
    fun delete(producto: Usuario)
}