package mx.edu.itm.link.t_login.BaseDeDatos

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName="materias")
class Materia (
    val nombre_materia:String,
    val calificacion: Float,


    //Aqui va la llave foranea
    @NonNull
    @ColumnInfo(name = "usuario_id")
    public val usuario_id:String,


    //esta es la llave primaria
    @NonNull
    @PrimaryKey(autoGenerate = false)
    var id_materia:Int=0
        ):Serializable
