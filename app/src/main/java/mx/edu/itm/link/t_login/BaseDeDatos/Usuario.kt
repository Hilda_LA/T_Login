package mx.edu.itm.link.t_login.BaseDeDatos

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName="usuarios")
class Usuario (

    val nombre:String,
    val carrera:String,
    val contraseña: String,


    @PrimaryKey(autoGenerate = false)
    var id_alumno:Int=0
        ): Serializable